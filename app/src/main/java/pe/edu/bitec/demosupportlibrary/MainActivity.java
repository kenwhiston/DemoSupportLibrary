package pe.edu.bitec.demosupportlibrary;

import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Toast;

import java.util.ArrayList;

import pe.edu.bitec.demosupportlibrary.adapter.ContactoAdapter;
import pe.edu.bitec.demosupportlibrary.clases.Contacto;

public class MainActivity extends AppCompatActivity {

    private RecyclerView rcLista;
    private ArrayList<Contacto> contactos;
    private ContactoAdapter contactoAdapter;
    private FloatingActionButton fab;
    private CoordinatorLayout lyFondo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        lyFondo = findViewById(R.id.lyFondo);
        fab = findViewById(R.id.fab);
        rcLista = findViewById(R.id.rcLista);

        //orientacion de la lista
        LinearLayoutManager linearLayoutManager =
                new LinearLayoutManager(this,
                        LinearLayoutManager.VERTICAL,
                        false);

        //asignamos la orientacion
        rcLista.setLayoutManager(linearLayoutManager);

        contactos = new ArrayList<>();

        contactos.add(new Contacto(1,"Whiston Borja",R.mipmap.ic_launcher));
        contactos.add(new Contacto(2,"Diego Echevarria",R.mipmap.ic_launcher));
        contactos.add(new Contacto(3,"Jerry Oyola",R.mipmap.ic_launcher));
        contactos.add(new Contacto(1,"Whiston Borja",R.mipmap.ic_launcher));
        contactos.add(new Contacto(2,"Diego Echevarria",R.mipmap.ic_launcher));
        contactos.add(new Contacto(3,"Jerry Oyola",R.mipmap.ic_launcher));
        contactos.add(new Contacto(1,"Whiston Borja",R.mipmap.ic_launcher));
        contactos.add(new Contacto(2,"Diego Echevarria",R.mipmap.ic_launcher));
        contactos.add(new Contacto(3,"Jerry Oyola",R.mipmap.ic_launcher));
        contactos.add(new Contacto(1,"Whiston Borja",R.mipmap.ic_launcher));
        contactos.add(new Contacto(2,"Diego Echevarria",R.mipmap.ic_launcher));
        contactos.add(new Contacto(3,"Jerry Oyola",R.mipmap.ic_launcher));
        contactos.add(new Contacto(1,"Whiston Borja",R.mipmap.ic_launcher));
        contactos.add(new Contacto(2,"Diego Echevarria",R.mipmap.ic_launcher));
        contactos.add(new Contacto(3,"Jerry Oyola",R.mipmap.ic_launcher));
        contactos.add(new Contacto(1,"Whiston Borja",R.mipmap.ic_launcher));
        contactos.add(new Contacto(2,"Diego Echevarria",R.mipmap.ic_launcher));
        contactos.add(new Contacto(3,"Jerry Oyola",R.mipmap.ic_launcher));
        contactos.add(new Contacto(1,"Whiston Borja",R.mipmap.ic_launcher));
        contactos.add(new Contacto(2,"Diego Echevarria",R.mipmap.ic_launcher));
        contactos.add(new Contacto(3,"Jerry Oyola",R.mipmap.ic_launcher));
        contactos.add(new Contacto(1,"Whiston Borja",R.mipmap.ic_launcher));
        contactos.add(new Contacto(2,"Diego Echevarria",R.mipmap.ic_launcher));
        contactos.add(new Contacto(3,"Jerry Oyola",R.mipmap.ic_launcher));
        contactos.add(new Contacto(1,"Whiston Borja",R.mipmap.ic_launcher));
        contactos.add(new Contacto(2,"Diego Echevarria",R.mipmap.ic_launcher));
        contactos.add(new Contacto(3,"Jerry Oyola",R.mipmap.ic_launcher));
        contactos.add(new Contacto(1,"Whiston Borja",R.mipmap.ic_launcher));
        contactos.add(new Contacto(2,"Diego Echevarria",R.mipmap.ic_launcher));
        contactos.add(new Contacto(3,"Jerry Oyola",R.mipmap.ic_launcher));


        contactoAdapter = new ContactoAdapter(contactos);

        rcLista.setAdapter(contactoAdapter);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(lyFondo, "Hola Brandon Lee....", Snackbar.LENGTH_LONG).
                        setAction("Acción", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Toast.makeText(MainActivity.this, "Acción Snackbar", Toast.LENGTH_LONG).show();
                    }
                }).show();
            }
        });



    }
}
