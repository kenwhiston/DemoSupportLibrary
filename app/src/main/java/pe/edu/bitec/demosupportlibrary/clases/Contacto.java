package pe.edu.bitec.demosupportlibrary.clases;

import java.io.Serializable;

public class Contacto implements Serializable {

    private int id;
    private String apenom;
    private int idFoto;

    public Contacto() {

    }

    public Contacto(int id, String apenom, int idFoto) {
        this.id = id;
        this.apenom = apenom;
        this.idFoto = idFoto;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getApenom() {
        return apenom;
    }

    public void setApenom(String apenom) {
        this.apenom = apenom;
    }

    public int getIdFoto() {
        return idFoto;
    }

    public void setIdFoto(int idFoto) {
        this.idFoto = idFoto;
    }
}
