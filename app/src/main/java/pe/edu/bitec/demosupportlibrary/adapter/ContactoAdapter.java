package pe.edu.bitec.demosupportlibrary.adapter;

import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import pe.edu.bitec.demosupportlibrary.R;
import pe.edu.bitec.demosupportlibrary.clases.Contacto;

public class ContactoAdapter extends RecyclerView.Adapter<ContactoAdapter.ViewHolder>
        implements View.OnClickListener{

    private ArrayList<Contacto> data;

    public ContactoAdapter(ArrayList<Contacto> data) {
        this.data = data;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.
                from(parent.getContext()).
                inflate(R.layout.ly_item_contacto,parent,false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Contacto contacto = this.data.get(position);

        holder.txtApenom.setText(contacto.getApenom());
        Picasso.get().load(contacto.getIdFoto()).into(holder.img);

        //agregar el oyente
        holder.lyFondo.setOnClickListener(this);
        holder.lyFondo.setTag(position);
    }

    @Override
    public int getItemCount() {
        return this.data.size();
    }

    @Override
    public void onClick(View view) {
        if(view.getId() == R.id.lyFondo){
            int pos = Integer.parseInt(view.getTag().toString());
            Contacto itemBluetooth = data.get(pos);
            //Intent intent = new Intent(view.getContext(), DatosEnviadosActivity.class);
            //Intent intent = new Intent(view.getContext(), DatosRecibidosActivity.class);
            //intent.putExtra("ITEM",itemBluetooth);
            //view.getContext().startActivity(intent);
        }
    }
    class ViewHolder extends RecyclerView.ViewHolder{

        RelativeLayout lyFondo;
        TextView txtApenom;
        ImageView img;

        public ViewHolder(View itemView) {

            super(itemView);

            txtApenom = (TextView) itemView.findViewById(R.id.txtApenom);
            img = (ImageView) itemView.findViewById(R.id.img);
            lyFondo = (RelativeLayout) itemView.findViewById(R.id.lyFondo);

        }
    }

}
